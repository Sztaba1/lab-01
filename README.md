# Lab 1. Programming Language Theory: Arithmetic

Laboratory created by Mateusz Ślażyński for "Programming Language Theory" course at AGH University of Science and Technology in Kraków.

This class concerns a basic arithmetic language.
Students are expected to implement the semantics of the language.

The "TAPL book" referenced in the source code can be found [here](https://github.com/MPRI/M2-4-2/blob/master/Types%20and%20Programming%20Languages.pdf).

## Lab Instructions

Replace TODO: comments in files:

- `src/semantics.py` — how to evaluate terms?
- `src/term.py` — how to identify whether a term is a value or number?

The related excerpts from the book are in the following files:

- [booleans](https://gitlab.com/agh-courses/23/iplt/lab-01/-/raw/main/01_booleans.png)
- [arithmetic](https://gitlab.com/agh-courses/23/iplt/lab-01/-/raw/main/02_arithmetic.png)

## How To Run Examples?

- `python main.py examples/01_pred.nb`
- `python main.py examples/02_ifelse.nb`
- `python main.py examples/03_stuck.nb`

## Setup 

* [ ] Make sure, you have a **private** group 
  * [how to create a group](https://docs.gitlab.com/ee/user/group/#create-a-group)
* [ ] Fork this project into your private group
  * [how to create a fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
* [ ] Add @bobot-is-a-bot as the new project's member (role: **maintainer**)
  * [how to add an user](https://docs.gitlab.com/ee/user/project/members/index.html#add-a-user)
  
## How To Submit Solutions

* [ ] Clone repository: git clone:
    ```bash 
    git clone <repository url>
    ```
* [ ] Solve the exercises 
    * use MiniZincIDE, whatever
* [ ] Commit your changes
    ```bash
    git add <path to the changed files>
    git commit -m <commit message>
    ```
* [ ] Push changes to the gitlab master branch
    ```bash
    git push -u origin master
    ```

The rest will be taken care of automatically. You can check the `GRADE.md` file for your grade / test results. Be aware that it may take some time (up to one hour) till this file appears / gets updated.  



 
