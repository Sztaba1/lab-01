#  Copyright (c) 2021-2021. Created by Mateusz Slazynski for the educational purposes.
#     Feel free to use/modify this code for any greater good.
#     It would be nice however if you mentioned me somewhere.
#     Still, no pressure - have a nice day!

from __future__ import annotations
from src.term import *
from dataclasses import dataclass
from enum import Enum, auto

class Rule(Enum):
    '''
    Enum representing various semantic rules.
    '''
    IfTrue = auto()
    IfFalse = auto()
    If = auto()
    Succ = auto()
    PredZero = auto()
    PredSucc = auto()
    Pred = auto()
    IsZeroZero = auto()
    IsZeroSucc = auto()
    IsZero = auto()

@dataclass(frozen=True)
class Transition:
    '''
    Class representing state transitions
    
    Attributes:
    ----------
    old_state: Term
        previous state
    new_state: Term
        a new state
    rule: Rule
        which rule was used to make the transition
    witnesses: tuple[Transistion, ...]
        list of transition that were required to make this one happen 
    '''
    old_state: Term
    new_state: Term
    rule: Rule
    witnesses: tuple[Transition, ...] = tuple()

class NoRuleApplies(Exception):
    '''
    Exception to be thrown when the evaluation ends.
    '''
    pass

class ArithmeticSemantics:

    @staticmethod
    def single_step(state_before: Term) -> Transition:
        '''
            Function applying correct rule to the given term.
        '''

        def transition(new_state: Term, rule: Rule, witnesses: tuple[Transition, ...] = ()):
            '''
                Just a helper function to create transition objects.
            '''
            return Transition(state_before, new_state, rule, witnesses)

        match state_before:
            # TODO:
            # match cases according to the chapter 4 of the book, function eval1
            # main difference:
            # - instead of returning term you should return a transition object (using the transition function helper)
            # - each transition can also store results of the intermediate steps, i.e. if a rule requires another rule
            #   to match, it should store this transition in the "witnesses" tuple
            # transitions are later used to display a derivation tree
            case _:
                raise NoRuleApplies()